#ifndef _SYSCALL_ADAPTER_H_
#define _SYSCALL_ADAPTER_H_

#define __NR_clock_gettime 113
#define __NR_clock_getres 114
#define __NR_gettimeofday 169

#endif