
#include <android-base/strings.h>
#include <benchmark/benchmark.h>
#include <dlfcn.h>

#include "util.h"

void local_function() {}

template<typename F>
static int bm_dladdr(F fun)
{
    const void* addr = reinterpret_cast<void*>(fun);
    Dl_info info;
    int res = dladdr(addr, &info);
    if (res == 0) abort();
    if (info.dli_fname == nullptr) abort();
    return res;
}

void BM_dladdr_local_function(benchmark::State& state) {
  while (state.KeepRunning()) {
    benchmark::DoNotOptimize(bm_dladdr(local_function));
  }
}

BIONIC_BENCHMARK(BM_dladdr_local_function);