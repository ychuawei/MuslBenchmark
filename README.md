
### 下载代码
Openharmony代码切换到third_party/musl目录执行：
~~~
git clone https://gitee.com/ychuawei/MuslBenchmark.git
~~~

### 编译benchmark

1、将`MuslBenchmark/Benchmark/gn.patch`的修改apply到musl根目录下的BUILD.gn中（apply失败的话可手动添加如下代码）
~~~
group("benchmark-musl") {
    deps = [ "MuslBenchmark/Benchmark/musl:musl_benchmark" ]
}

group("benchmark-bionic") {
    deps = [ "MuslBenchmark/Benchmark/bionic:bionic_benchmark" ]
}
~~~
2、./build.sh --product-name device-name(e.g. rk3568  hispark_taurus_standard) `--build-target benchmark-musl --build-target benchmark-bionic`

benchmark-musl：编译musl新增用例

benchmark-bionic：编译bionic自身用例

输出产物：bionic_benchmark、musl_benchmark

### 运行

1、out目录下寻找bionic_benchmark、musl_benchmark，push到设备中，赋予可执行权限，运行
~~~
mount -o rw,remount /
chmod +x xxx
~~~

2、常用参数

musl benchmark使用：
~~~
设置跑在哪个核上

--musl_cpu=6

跑一部分用例

--benchmark_filter="xxx"

设置循环次数

--musl_iterations=
~~~

bionic benchmark使用：
~~~
--bionic_cpu：功能同上

--bionic_iterations：功能同上
~~~

### 比较俩次运行结果
场景：用于比较俩次跑benchmark的结果，可以看到性能的变化

输入：before.txt 、after.txt的内容都是直接跑benchmark的输出结果，如
~~~
2023-05-31T06:57:50+00:00
Running /data/local/tmp/bionic_benchmark
Run on (8 X 2045 MHz CPU s)
Load Average: 43.31, 43.00, 37.36
***WARNING*** CPU scaling is enabled, the benchmark real time measurements may be noisy and will incur extra overhead.
------------------------------------------------------------------------------------------------------------
Benchmark                                                  Time             CPU   Iterations UserCounters...
------------------------------------------------------------------------------------------------------------
BM_string_memmove_overlap_dst_before_src/8/0            3.33 ns         3.30 ns    191351480 bytes_per_second=2.2567G/s
BM_string_memmove_overlap_dst_before_src/512/0          16.8 ns         16.6 ns     41964335 bytes_per_second=28.6489G/s
~~~
执行：./compare_result.py --before before.txt --after after.txt

输出：+表示after优于before

~~~
|case                                                        |before              |after                       |(before - after / before)       |
|----                                                        |----                |----                        |----                            |
|BM_string_memmove_overlap_dst_before_src/16384/0            |603.0               |616.0                       |-2.16%                          |
|BM_string_memmove_overlap_dst_before_src/512/0              |16.8                |16.7                        |+0.6%                           |
~~~