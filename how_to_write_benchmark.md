# Benchmark框架常用参数介绍
- `--musl_cpu=<cpu_to_isolate>`: 设置benchmark用例跑在哪个核上
- `--musl_iterations=<num_iter>`： 设置benchmark用例的迭代次数
- `--benchmark_list_tests={true|false}`: 列出所有的用例
- `--benchmark_filter=<regex>`：正则设置要跑的用例
- `--benchmark_format=<console|json|csv>`：设置输出的格式
- `--benchmark_out=<filename>`：设置输出的文件名字
- `--v=<verbosity>`：设置调试等级，当某个用例挂了，不知道跑到哪个用例的时候可以使用
- 其他参数可使用`--help`查看
# 测试用例如何编写

demo：
~~~
static void Bm_function_Mmap_anonymous(benchmark::State &state)
{
    size_t length = state.range(0); // 获取第一个参数
    for (auto _ : state) { // for循环为性能测试点
        char* mem = (char *)mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
        if (mem != MAP_FAILED) {
            benchmark::DoNotOptimize(mem); // 防止优化
            state.PauseTiming(); // 去除不关心的耗时（开始）
            munmap(mem, length);
            state.ResumeTiming();// 去除不关心的耗时（结束）
        }
    }
    state.SetItemsProcessed(state.iterations());
}


~~~


## 控制计时器
通常for循环内部为性能测试阶段，但我们有时候需要去除一些不关心的代码逻辑可使用下面2个接口。
这俩个接口耗时大概1000ns左右，需要考虑用例如果用时更小不建议加 [暂停计时，恢复计时]。
- `state.PauseTiming();`
- `state.ResumeTiming();`

## 传递参数
### 获取外部参数
`state.range(n);` // n从0开始

### 传单个参数
Range可以传一个start和end，产生一个范围内的多个参数，RangeMultiplier函数可以定义一个倍率
- `BENCHMARK(BM_xxx)->Arg(8)->Arg(64)->Arg(512)->Arg(1<<10)->Arg(8<<10)`: 产生多用例，每个用例传一个参数
- `BENCHMARK(BM_xxx)->Range(8, 8<<10)`：[ 8, 64, 512, 4k, 8k ]
- `BENCHMARK(BM_xxx)->RangeMultiplier(2)->Range(8, 8<<10)`: [ 8, 16, 32, 64, 128, 256, 512, 1024, 2k, 4k, 8k ]
- `BENCHMARK(BM_xxx)->DenseRange(start, end, step)`: (0， 1024， 128) -> [ 0, 128, 256, 384, 512, 640, 768, 896, 1024 ]

注：

    倍率默认是8，范围参数产生的逻辑是，先把start放到参数里面，然后使用1 * 倍率，当得到的数值大于start的时候会把它放到参数里面

举例：

    如BENCHMARK(BM_xxx)->Range(11, 8<<10)输出为：
    BM_xxx/11
    BM_xxx/64
    BM_xxx/512
    BM_xxx/4096
    BM_xxx/8192
    start传入的是11，那么第一个参数是11，第二个参数是1 * 8 * 8而不是11 * 8

### 传多个参数
- `BENCHMARK(BM_xxx)->Args({8， 10})->Args({64， 10})`: 产生多用例，每个用例传2个参数
- 自定义传参函数，然后使用`Apply`接口
~~~
static void CustomArguments(benchmark::internal::Benchmark* b) {
  for (int i = 0; i <= 10; ++i)
    for (int j = 32; j <= 1024*1024; j *= 8)
      b->Args({i, j});
}
BENCHMARK(BM_xxx)->Apply(CustomArguments);
~~~

### 传字符串或者浮点数
上述benchmark接口只能接受`int64`类型的参数，如果想生成带浮点数或者字符串参数的多用例，可以参考如下实现方式：

- 1、在用例中提前定义好字符串数组
- 2、使用`MUSL_BENCHMARK_WITH_ARG`传入`BNECHMARK_n`参数生成多个用例
~~~
MUSL_BENCHMARK_WITH_ARG(Bm_function_Strtod, "BNECHMARK_n")

等价于：BENCHMARK(Bm_function_Strtod)->Arg(0)->Arg(1)->Arg(n-1)
~~~
目前在`benchmark_framework.cpp`中已经预置了`BNECHMARK_5`、`BNECHMARK_8`，如有需要可自由扩充

- 3、读取外部传入的arg，然后从数组对应位置取到字符串

~~~
static void Bm_function_Strtod(benchmark::State &state)
{
    const char *var[] = {"+2.86500000e+01", "3.1415", "29",
                         "-123.456", "1.23e5", "0x1.2p3",
                         "-inf", "123foo"};
    const char *str = var[state.range(0)];
    char *ptr;
    for (auto _ : state) {
        benchmark::DoNotOptimize(strtod(str, &ptr));
    }
}

MUSL_BENCHMARK_WITH_ARG(Bm_function_Strtod, "BNECHMARK_8");
~~~

- 4、使用`MUSL_BENCHMARK_WITH_APPLY`传入`ApplyBenchmarkFunc`参数生成多个用例
~~~
MUSL_BENCHMARK_WITH_APPLY(Bm_function_Memchr, StringtestArgs)

等价于：BENCHMARK(Bm_function_Memchr)->Apply(StringtestArgs)
~~~

## 设置名字

### 设置用例名字
- 使用Name设置用例名字：`BENCHMARK(BM_xxx)->Name("test");`

### 设置用例参数名字
- 使用ArgName设置参数：`BENCHMARK(BM_arg_name)->Arg(3)->ArgName("first");`
- 使用ArgNames设置多个参数：`BENCHMARK(BM_arg_names)->Args({1, 2, 3})->ArgNames({"first", "", "third"});`

### 设置用例Label
- 使用`state.SetLabel()`为用例设置一个标识

### 效果展示
~~~c++
static void Bm_function_max(benchmark::State &state)
{
    int arg1 = state.range(0);
    int arg2 = state.range(0);
    for (auto _ : state) {
        benchmark::DoNotOptimize(max(arg1, arg2));
    }
}

static void Bm_function_max_with_info(benchmark::State &state)
{
    int arg1 = state.range(0);
    int arg2 = state.range(0);
    state.SetLabel("label: max test"); // 设置label
    for (auto _ : state) {
        benchmark::DoNotOptimize(max(arg1, arg2));
    }
}

BENCHMARK(Bm_function_max)->Args({1 , 2});
BENCHMARK(Bm_function_max_with_info)->Args({1 , 2})->ArgNames({"first, second"});

Benchmark                                            Time             CPU   Iterations
--------------------------------------------------------------------------------------
Bm_function_max/1/2                               1.19 ns         1.18 ns    593959607
Bm_function_max_with_info/first, second:1/2       1.21 ns         1.19 ns    594025263 label: max test
~~~

### 配置json文件
~~~json
{
	"InterfaceUsecases": [{
		"name": "Bm_function_Tolower_a",
		"iterations": 1000,
		"cpu": 3
	}, {
		"name": "Bm_function_Tolower_A",
		"iterations": 1000,
		"cpu": 3
	}, {
		"name": "Bm_function_Tolower_all_ascii",
		"iterations": 1000,
		"cpu": 3
	}, {
		"name": "Bm_function_Strchrnul",
		"args": "ALIGNED_ONEBUF",
		"iterations": 1000,
		"cpu": 3
	}, {
		"name": "Bm_function_Memchr"
	}]
}
~~~

## 匹配json文件中的json通配符
- 支持匹配形如"Bm_function_xxx_*"的json通配符
- 若匹配的字符串中没有通配符则直接判断俩字符串是否相等并返回，即"return str == pattern"




More info: https://gitee.com/openharmony/third_party_benchmark/blob/master/docs/user_guide.md